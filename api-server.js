var http  = require('http');
var port  = process.env.PORT  || 8888;
var delay = process.env.DELAY || 1000;

http.createServer(function(req, res) {
  var resp;
  
  if (/array/.test(req.url)) {
    console.log('Respond with Array');
    resp = [
      {id: 1, name: 'obj1'}, 
      {id: 2, name: 'obj2'},
      {id: 3, name: 'obj3'}
    ];
  }

  if (/object/.test(req.url)) {
    console.log('Respond with Object');
    resp = {id: 1, name: 'obj1'};
  }

  setTimeout(function() {
    res.writeHead(200, {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    });
    res.end(JSON.stringify(resp));
  }, 1000);
})  
  .listen(port, function(){
    console.log('started', port);
  });