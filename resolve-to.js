
angular.module('resolveTo', [])

  .factory('resolveTo', function() {

    var resolvers = {};

    function prop(target, name, value) {
      if (Object.defineProperty) {
        Object.defineProperty(target, name, {
          value    : value,
          writable : true,
          enumerable: false
        });
      } else {
        target[name] = value;
      }
    }

    function method(result, populate) {

      return function(promise) {
        prop(result, '$promise',  promise);
        prop(result, '$done',     false);
        prop(result, '$resolved', false);
        prop(result, '$rejected', false);

        promise.then(function(resp) {
          populate(result, resp.data);
          result.$done     = true;
          result.$resolved = true;
        }, function() {
          result.$done     = true;
          result.$rejected = true;
        });

        return result;
      };
    }

    resolvers.array = method([], function(result, arr) {
      result.splice.apply(result, [0, 0].concat(arr));
    });

    resolvers.object = method({}, function(result, obj) {
      for (var p in obj) { if (obj.hasOwnProperty(p)) {
        result[p] = obj[p];
      }};
    });

    return resolvers;
  });
  
 